packtivity-validate steps.yml#/skimming
packtivity-validate steps.yml#/scaling
packtivity-validate steps.yml#/fitting

packtivity-run steps.yml#/skimming -p input_file="'{workdir}/inputdata/DAOD_EXOT27.20140688._000071.pool.root.1'" -p output_file="'{workdir}/selected.root'"
rm -r _packtivity

packtivity-run steps.yml#/scaling  -p input_file="'{workdir}/selected.root'" -p output_file="'{workdir}/signal_scaled.root'" -p validation_plot="'{workdir}/signal_scaled.png'" -p cross_section=44.873 -p sum_of_weights=6813.025800 -p k_factor=1 -p filter_eff=1 -p luminosity=140.1 -p hist='h_mjj'
rm -r _packtivity

packtivity-run steps.yml#/fitting  -p eosuser='meehan' -p eospass='Moxze-20' -p filedata='root://eosuser.cern.ch//eos/user/m/meehan/ATLASRecast2021/external_data.root' -p histdata='data' -p filebkg='root://eosuser.cern.ch//eos/user/m/meehan/ATLASRecast2021/external_data.root' -p histbkg='background' -p filesig="'{workdir}/signal_scaled.root'" -p histsig='h_mjj' -p outputfile="'{workdir}/limits.png'" -p plotfile="'{workdir}/spectrum.png'"
rm -r _packtivity